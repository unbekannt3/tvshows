export class TVShow {

  coverURL: string;
  summary: string;
  id: string;

  constructor(
    public name?: string
  ) {
  }

}
