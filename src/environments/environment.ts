// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD4hwWRUBCFdaYQ12RhJQZxuuN0DFHi_E8',
    authDomain: 'tvshows-46076.firebaseapp.com',
    databaseURL: 'https://tvshows-46076.firebaseio.com',
    projectId: 'tvshows-46076',
    storageBucket: 'tvshows-46076.appspot.com',
    messagingSenderId: '611095959467',
    appId: '1:611095959467:web:5d7050876f88d1b58924ee'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
